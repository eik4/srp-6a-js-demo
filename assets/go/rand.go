package main

import (
  "fmt"
  "math/rand"
  "time"
)

func randInt(min int, max int) int {
  rand.Seed(time.Now().UTC().UnixNano())
  return min + rand.Intn(max-min)
}

func randomHexString(l int) string {
  letters := [16]string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
  str := "";
  for i := 0; i < l; i++ {
    if i == 0 {
      str = str + letters[randInt(1, 15)]
    } else {
      str = str + letters[randInt(0, 15)]
    }
  }
  return str;
}

func main() {
  fmt.Println(randomHexString(64))
}
