package main

import (
  "fmt"
  "encoding/hex"
  "regexp"
  "math/big"
  "math/rand"
  "time"
  "hash"
  "crypto/sha256"
  "strconv"
  //"reflect"
)

func padTo(bytes []byte, length int) []byte {
  paddingLength := length - len(bytes)
  padding := make([]byte, paddingLength, paddingLength)

  return append(padding, bytes...)
}

func padToN(number *big.Int) []byte {
  return padTo(number.Bytes(), 4096/8)
}

func hashToBytes(h hash.Hash) []byte {
  return h.Sum(nil)
}

func hashToInt(h hash.Hash) *big.Int {
  U := new(big.Int)
  U.SetBytes(hashToBytes(h))
  return U
}

func intFromBytes(bytes []byte) *big.Int {
  i := new(big.Int)
  i.SetBytes(bytes)
  return i
}

func intToBytes(i *big.Int) []byte {
  return i.Bytes()
}

func bytesFromHexString(s string) []byte {
  re, _ := regexp.Compile("[^0-9a-fA-F]")
  h := re.ReplaceAll([]byte(s), []byte(""))
  b, _ := hex.DecodeString(string(h))
  return b
}

func randInt(min int, max int) int {
  rand.Seed(time.Now().UTC().UnixNano())
  return min + rand.Intn(max-min)
}

func randomHexString(l int) string {
  letters := "0123456789abcdef";
  str := "";
  for i := 0; i < l; i++ {
    if i == 0 {
      str = str + string(letters[randInt(1, 15)])
    } else {
      str = str + string(letters[randInt(0, 15)])
    }
  }
  //fmt.Println(str);
  return str;
}

func randomString(min int, max int) string {
  letters := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  str := "";
  for i := 0; i < randInt(min, max); i++ {
    if i == 0 {
      str = str + string(letters[randInt(1, len(letters))])
    } else {
      str = str + string(letters[randInt(0, len(letters))])
    }
  }
  //fmt.Println(str);
  return str;
}

func my_xor(s1 string, s2 string) string {
  n1, err1 := strconv.ParseInt(s1, 16, 64)
  n2, err2 := strconv.ParseInt(s2, 16, 64)
  if err1 == nil && err2 == nil {
    return strconv.FormatInt(n1^n2, 16)
  } else {
    return "";
  }
}

func xorWithLeadZeros(s1 string, s2 string) string {
  result := "";
  if len(s1) != len(s2) {
    fmt.Println("unequal lengths of strings\n");
    return result;
  }
  for i := 0; i < len(s1); i++ {
    result = result + my_xor(string(s1[i]), string(s2[i]));
  }
  return result;
}

func main () {
  N := intFromBytes(bytesFromHexString(`
    FFFFFFFF FFFFFFFF C90FDAA2 2168C234 C4C6628B 80DC1CD1 29024E08
    8A67CC74 020BBEA6 3B139B22 514A0879 8E3404DD EF9519B3 CD3A431B
    302B0A6D F25F1437 4FE1356D 6D51C245 E485B576 625E7EC6 F44C42E9
    A637ED6B 0BFF5CB6 F406B7ED EE386BFB 5A899FA5 AE9F2411 7C4B1FE6
    49286651 ECE45B3D C2007CB8 A163BF05 98DA4836 1C55D39A 69163FA8
    FD24CF5F 83655D23 DCA3AD96 1C62F356 208552BB 9ED52907 7096966D
    670C354E 4ABC9804 F1746C08 CA18217C 32905E46 2E36CE3B E39E772C
    180E8603 9B2783A2 EC07A28F B5C55DF0 6F4C52C9 DE2BCBF6 95581718
    3995497C EA956AE5 15D22618 98FA0510 15728E5A 8AAAC42D AD33170D
    04507A33 A85521AB DF1CBA64 ECFB8504 58DBEF0A 8AEA7157 5D060C7D
    B3970F85 A6E1E4C7 ABF5AE8C DB0933D7 1E8C94E0 4A25619D CEE3D226
    1AD2EE6B F12FFA06 D98A0864 D8760273 3EC86A64 521F2B18 177B200C
    BBE11757 7A615D6C 770988C0 BAD946E2 08E24FA0 74E5AB31 43DB5BFC
    E0FD108E 4B82D120 A9210801 1A723C12 A787E6D7 88719A10 BDBA5B26
    99C32718 6AF4E23C 1A946834 B6150BDA 2583E9CA 2AD44CE8 DBBBC2DB
    04DE8EF9 2E8EFC14 1FBECAA6 287C5947 4E6BC05D 99B2964F A090C3A2
    233BA186 515BE7ED 1F612970 CEE2D7AF B81BDD76 2170481C D0069127
    D5B05AA9 93B4EA98 8D8FDDC1 86FFB7DC 90A6C08F 4DF435C9 34063199
    FFFFFFFF FFFFFFFF
  `));
  g := big.NewInt(5);
  k := sha256.New();
  k.Write([]byte(N.String()));
  k.Write([]byte(g.String()));
  
  I := "alice";
  p := "password123";
  s := "salty";
  
  //I := randomString(6,20);
  //p := randomString(6,20);
  //s := randomString(6,20);
  
  fmt.Println("I: ", I);
  fmt.Println("p: ", p);
  fmt.Println("s: ", s);
  
  lp1 := sha256.New();
  lp1.Write([]byte(I));
  lp1.Write([]byte(":"));
  lp1.Write([]byte(p));
  
  ss1 := sha256.New();
  ss1.Write([]byte(s));
  
  x := sha256.New();
  x.Write(([]byte(fmt.Sprintf("%x", ss1.Sum(nil)))));
  x.Write(([]byte(fmt.Sprintf("%x", lp1.Sum(nil)))));
  
  v := big.NewInt(0);
  v.Exp(g, hashToInt(x), N);
  
  //a_gen := randomHexString(64);
  a_gen := "68b7bd2cd98ac25e2e67cc7a31b637e63e58b2ab577bd89aa17a312343c78404";
  fmt.Println("a: ", a_gen);
  a := intFromBytes(bytesFromHexString(a_gen));
  A := big.NewInt(0);
  A.Exp(g, a, N);
  fmt.Println("A: ", fmt.Sprintf("%x", A));
  
  //b_gen := randomHexString(64);
  b_gen := "e777bd067c6929785c39be32c76eddd31ebcdadd5804ee29e371c972391d5040";
  fmt.Println("b: ", b_gen);
  b := intFromBytes(bytesFromHexString(b_gen));
  B := big.NewInt(0);
  B.Exp(g, b, N);
  
  left := big.NewInt(0);
  left = left.Mod(hashToInt(k), N);
  left = left.Mul(left, v);
  left = left.Mod(left, N);
  left = left.Add(left, B);
  
  B = B.Mod(left, N);
  fmt.Println("B: ", fmt.Sprintf("%x", B));
  
  u := sha256.New();
  u.Write([]byte(A.String()));
  u.Write([]byte(B.String()));
  //fmt.Println(fmt.Sprintf("%x\n", u.Sum(nil)));
  
  us1 := big.NewInt(0);
  us1 = us1.Exp(g, hashToInt(x), N);
  us1 = us1.Mul(us1, hashToInt(k));
  us1 = us1.Mod(us1, N);
  us1 = us1.Sub(B, us1);
  us2 := big.NewInt(0);
  us2 = us2.Mul(hashToInt(u), hashToInt(x));
  us2 = us2.Mod(us2, N);
  us2 = us2.Add(a, us2);
  us2 = us2.Mod(us2, N);
  us := big.NewInt(0);
  us = us.Exp(us1, us2, N);
  
  uk := sha256.New();
  uk.Write([]byte(us.String()));
  uk_str := fmt.Sprintf("%x", uk.Sum(nil));
  fmt.Println("uk:", uk_str);
  
  hs := big.NewInt(0);
  hs = hs.Exp(v, hashToInt(u), N);
  hs = hs.Mul(A, hs);
  hs = hs.Mod(hs, N);
  hs = hs.Exp(hs, b, N);
  
  hk := sha256.New();
  hk.Write([]byte(hs.String()));
  hk_str := fmt.Sprintf("%x", hk.Sum(nil));
  fmt.Println("hk:", hk_str);
  fmt.Println(fmt.Sprintf("checkSRP({I: '%s', p: '%s', s: '%s', a: '%s', b: '%s', K: '%s'})\n", I, p, s, a_gen, b_gen, hk_str));
  
  //result := (fmt.Sprintf("%x", hk.Sum(nil)) == fmt.Sprintf("%x", uk.Sum(nil)));
  //fmt.Println(result);
  
  //hash_N := sha256.New();
  //hash_N.Write([]byte(N.String()));
  //
  //hash_g := sha256.New();
  //hash_g.Write([]byte(g.String()));
  //
  //xor_Ng := xorWithLeadZeros(fmt.Sprintf("%x", hash_N.Sum(nil)), fmt.Sprintf("%x", hash_g.Sum(nil)));
  //
  //hash_I := sha256.New();
  //hash_I.Write([]byte(I));
  //
  //uM := sha256.New();
  //uM.Write([]byte(xor_Ng));
  //uM.Write([]byte(fmt.Sprintf("%x", hash_I.Sum(nil))));
  //uM.Write([]byte(s));
  //uM.Write([]byte(A.String()));
  //uM.Write([]byte(B.String()));
  //uM.Write([]byte(fmt.Sprintf("%x", uk.Sum(nil))));
  //fmt.Println("uM:",fmt.Sprintf("%x", uM.Sum(nil)));
  //
  //hM := sha256.New();
  //hM.Write([]byte(A.String()));
  //hM.Write([]byte(fmt.Sprintf("%x", uM.Sum(nil))));
  //hM.Write([]byte(fmt.Sprintf("%x", hk.Sum(nil))));
  //fmt.Println("hM:",fmt.Sprintf("%x", hM.Sum(nil)));
}
