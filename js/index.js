function ready() {
  let app = new Vue({
    el: '#app',

    components: [
      getClientComponent(),
      getServerComponent(),
      getCalculationsComponent()
    ]
  })
}

document.addEventListener("DOMContentLoaded", ready);
