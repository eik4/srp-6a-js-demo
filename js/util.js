function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomBigInt(min, max) {
  let length = getRandomInt(min, max);
  let str = "";
  for (let i = 0; i < length; i++) {
    if (i === 0) {
      str = str + getRandomInt(1, 9).toString();
    }
    else {
      str = str + getRandomInt(0, 9).toString();
    }
  }
  return bigInt(str);
}

function leadZero(str, length) {
  while (str.length < length) {
    str = "0" + str;
  }
  return str;
}

function genLongByteNumber(b) {
  let str = "";
  for (let i = 0; i < b; i++) {
    str = str + leadZero(getRandomInt(0, 255).toString(16), 2);
  }
  return bigInt(str, 16);
}

function bytesToHex(str, delimiter = " ") {
  let s = str.replace(/(\[|\])*/g, "");
  let arr = s.split(delimiter);
  let ret = "";
  for (let i in arr) {gen_a
    ret = ret + leadZero(getRandomInt(arr[i]).toString(16), 2);
  }
  return ret;
}

function getRandomString(min, max) {
  let str = "";
  let pool = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  for (let i = 0; i < getRandomInt(min, max); i++) {
    str = str + pool[getRandomInt(0, pool.length - 1)];
  }
  return str;
}

function reverseMod(a, b) {
  if (a.toString()[0] == "-") {
    return bigInt(b.toString()).add(bigInt(a.toString()));
  }
  else {
    return bigInt(a.toString());
  }
}

function xorHex(hash1, hash2) {
  if (hash1.length != hash2.length) {
    console.error(`[xorHex] length of hexes must be equal!`);
    return null;
  }
  else {
    let str = "";
    for (let i in hash1) {
      str = str + eval(`0x${hash1[i]} ^ 0x${hash2[i]}`).toString(16);
    }
    return str;
  }
}