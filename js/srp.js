// requires bigInt and hasing functions

class SRP {
  constructor(params = 0) {
    this.N = null;
    this.g = null;
    this.k = null;
    this.H = null;
    this.s = null;
    this.I = null;
    this.p = null;
    this.a = null;
    this.b = null;
    this.A = null;
    this.B = null;
    this.x = null;
    this.v = null;
    this.u = null;
    this.validParams = [4096];
    this.inited = false;
    if (params != 0) {
      this.setParams(params);
    }
  }

  setParams(params) {
    switch (params) {
      case 4096:
        this.N = bigInt(`
          FFFFFFFF FFFFFFFF C90FDAA2 2168C234 C4C6628B 80DC1CD1 29024E08
          8A67CC74 020BBEA6 3B139B22 514A0879 8E3404DD EF9519B3 CD3A431B
          302B0A6D F25F1437 4FE1356D 6D51C245 E485B576 625E7EC6 F44C42E9
          A637ED6B 0BFF5CB6 F406B7ED EE386BFB 5A899FA5 AE9F2411 7C4B1FE6
          49286651 ECE45B3D C2007CB8 A163BF05 98DA4836 1C55D39A 69163FA8
          FD24CF5F 83655D23 DCA3AD96 1C62F356 208552BB 9ED52907 7096966D
          670C354E 4ABC9804 F1746C08 CA18217C 32905E46 2E36CE3B E39E772C
          180E8603 9B2783A2 EC07A28F B5C55DF0 6F4C52C9 DE2BCBF6 95581718
          3995497C EA956AE5 15D22618 98FA0510 15728E5A 8AAAC42D AD33170D
          04507A33 A85521AB DF1CBA64 ECFB8504 58DBEF0A 8AEA7157 5D060C7D
          B3970F85 A6E1E4C7 ABF5AE8C DB0933D7 1E8C94E0 4A25619D CEE3D226
          1AD2EE6B F12FFA06 D98A0864 D8760273 3EC86A64 521F2B18 177B200C
          BBE11757 7A615D6C 770988C0 BAD946E2 08E24FA0 74E5AB31 43DB5BFC
          E0FD108E 4B82D120 A9210801 1A723C12 A787E6D7 88719A10 BDBA5B26
          99C32718 6AF4E23C 1A946834 B6150BDA 2583E9CA 2AD44CE8 DBBBC2DB
          04DE8EF9 2E8EFC14 1FBECAA6 287C5947 4E6BC05D 99B2964F A090C3A2
          233BA186 515BE7ED 1F612970 CEE2D7AF B81BDD76 2170481C D0069127
          D5B05AA9 93B4EA98 8D8FDDC1 86FFB7DC 90A6C08F 4DF435C9 34063199
          FFFFFFFF FFFFFFFF`.replace(/\s+/g, ""), 16);
        this.g = bigInt(5);
        this.H = sha256;
        this.k = this.H(`${this.N.toString(16)}${this.g.toString(16)}`);
        this.inited = true;
        break;
      default:
        console.error(`[srp] (create): initial params must be in ${this.validParams}`);
    }
  }

  clear() {
    this.s = null;
    this.I = null;
    this.p = null;
    this.a = null;
    this.b = null;
    this.A = null;
    this.B = null;
    this.x = null;
    this.v = null;
    this.u = null;
  }

  setLogin(str) {
    this.I = str;
  }

  setPassword(str) {
    this.p = str;
  }

  setSalt(str) {
    this.s = str;
  }

  gena() {
    this.a = genLongByteNumber(32);
    return this.a;
  }

  genb() {
    this.b = genLongByteNumber(32);
    return this.b;
  }

  calcx() {
    if (this.inited) {
      let flag = true;
      if (!this.I) {
        flag = false;
        console.error("[srp] (calcX) I is required");
      }
      if (!this.p) {
        flag = false;
        console.error("[srp] (calcX) p is required");
      }
      if (!this.s) {
        flag = false;
        console.error("[srp] (calcX) s is required");
      }
      if (flag) {
        let lp = this.H(`${this.I}${this.p}`);
        let ss = this.H(this.s);
        this.x = this.H(`${lp}${ss}`);
        return this.x;
      }
    }
    else {
      console.error(`[srp] (calcX) params must be set first`);
    }
    return null;
  }

  calcA() {
    if (this.inited) {
      let flag = true;
      if (!this.a) {
        flag = false;
        console.error("[srp] (calcA) a is required");
      }
      if (flag) {
        this.A = this.g.modPow(this.a, this.N);
        return this.A;
      }
    }
    else {
      console.error(`[srp] (calcA) params must be set first`);
    }
    return null;
  }

  calcB() {
    if (this.inited) {
      let flag = true;
      if (!this.b) {
        flag = false;
        console.error("[srp] (calcB) b is required");
      }
      if (!this.v) {
        flag = false;
        console.error("[srp] (calcB) v is required");
      }
      if (flag) {
        this.B = this.g.modPow(this.b, this.N);
        let left = bigInt(this.k, 16).mod(this.N);
        left = left.multiply(this.v);
        left = left.mod(this.N);
        left = left.add(this.B);
        this.B = left.mod(this.N);
        return this.B;
      }
    }
    else {
      console.error(`[srp] (calcB) params must be set first`);
    }
    return null;
  }

  setA(bigint) {
    this.A = bigint;
  }

  setB(bigint) {
    this.B = bigint;
  }

  setV(bigint) {
    this.v = bigint;
  }

  calcu() {
    if (this.inited) {
      let flag = true;
      if (!this.A) {
        flag = false;
        console.error("[srp] (calcu) A is required");
      }
      if (!this.B) {
        flag = false;
        console.error("[srp] (calcu) B is required");
      }
      if (flag) {
        this.u = this.H(`${this.A.toString(16)}${this.B.toString(16)}`);
        return this.u;
      }
    }
    else {
      console.error(`[srp] (calcu) params must be set first`);
    }
    return null;
  }

  calcClientKey() {
    if (this.inited) {
      let flag = true;
      if (!this.a) {
        flag = false;
        console.error("[srp] (calcClientKey) a is required");
      }
      if (flag) {
        if (!this.x) {
          console.warn(`[srp] (calcClientKey) attempting to calc x`);
          if (!this.calcx()) {
            return null;
          }
        }
        let us1 = this.g.modPow(bigInt(this.x, 16), this.N);
        us1 = us1.multiply(bigInt(this.k, 16));
        us1 = us1.mod(this.N);
        if (!this.B) {
          console.warn(`[srp] (calcClientKey) attempting to calc B`);
          if (!this.calcB()) {
            return null;
          }
        }
        us1 = this.B.subtract(us1);
        if (us1.toString()[0] == '-') {
          us1 = this.N.add(us1);
        }
        if (!this.A) {
          console.warn(`[srp] (calcClientKey) attempting to calc A`);
          if (!this.calcA()) {
            return null;
          }
        }
        if (!this.u) {
          console.warn(`[srp] (calcClientKey) attempting to calc u`);
          if (!this.calcu()) {
            return null;
          }
        }
        let us2 = bigInt(this.u, 16).multiply(bigInt(this.x, 16));
        us2 = us2.mod(this.N);
        us2 = us2.add(this.a);
        us2 = us2.mod(this.N);
        let us = us1.modPow(us2, this.N);
        let uk = this.H(`${us.toString(16).toString(16)}`);
        return uk;
      }
    }
    else {
      console.error(`[srp] (calcClientKey) params must be set first`);
    }
    return null;
  }

  calcServerKey() {
    if (this.inited) {
      let flag = true;
      if (!this.b) {
        flag = false;
        console.error("[srp] (calcClientKey) b is required");
      }
      if (!this.v) {
        flag = false;
        console.error("[srp] (calcClientKey) v is required");
      }
      if (!this.u) {
        flag = false;
        console.error("[srp] (calcClientKey) u is required");
      }
      if (flag) {
        let hs = bigInt(this.v).modPow(bigInt(this.u, 16), this.N);
        hs = hs.multiply(this.A);
        hs = hs.mod(this.N);
        hs = hs.modPow(this.b, this.N);
        let hk = this.H(`${hs.toString(16)}`);
        return hk;
      }
    }
    else {
      console.error(`[srp] (calcClientKey) params must be set first`);
    }
    return null;
  }

}

// srp = new SRP(4096);