function getClientComponent() {

  let clientComponent = Vue.component("client", {

    data: function () {
      return {
        login: "",
        password: "",
        answer: ""
      }
    },

    template: `<div class="client">
      <div class="form">
        <input type="text" v-bind:class="['input', {'error': login.trim() == ''}]" v-model="login" placeholder="Логин" @keypress="formKeyPress">
        <br>
        <input type="password" v-bind:class="['input', {'error': password.trim() == ''}]" v-model="password" placeholder="Пароль" @keypress="formKeyPress">
        <br>
        <div v-bind:class="['btn', {'disabled': !valid}]" @click="log">Войти</div>
        <div v-bind:class="['btn', {'disabled': !valid}]" @click="reg">Зарегистрироваться</div>
        <br>
        <span>{{answer}}</span>
      </div>
    </div>`,

    computed: {
      valid: function () {
        return this.login.trim() != "" && this.password.trim() != ""
      }
    },

    methods: {
      formKeyPress(e) {
        if (e.key == "Enter") {
          this.log();
        }
      },

      reg() {
        this.$parent.$children[2].clear();
        this.answer = "";
        if (this.valid) {
          let srp = new SRP(4096);
          let hash = srp.H(`${this.login}${this.password}`);
          this.$parent.$children[2].add({
            client: {
              key: "Вычисление хэша логина (I) и пароля (p)",
              value: hash
            }
          });
          this.$parent.$children[2].add({
            client: {
              full: "Отправление логина и хэша"
            },
            channel: {
              key: "Передача данных",
              value: `{login: '${this.login}', hash: '${hash}'}`
            },
            server: {
              full: 'Получение данных'
            }
          });
          this.$parent.$children[1].reg(this.login, this.password, hash);
          this.login = "";
          this.password = "";
        }
      },

      log() {
        this.$parent.$children[2].clear();
        this.answer = "";
        if (this.valid) {
          let srp = new SRP(4096);
          srp.setLogin(this.login);
          srp.setPassword(this.password);
          let tmp_a = srp.gena();
          this.$parent.$children[2].add({
            client: {
              key: "Генерирование секретного ключа (a)",
              value: `${tmp_a}`
            }
          });
          let A = srp.calcA();
          this.$parent.$children[2].add({
            client: {
              key: "Вычисление публичного ключа (A)",
              value: `${A.toString(16)}`
            }
          });
          this.$parent.$children[2].add({
            client: {
              full: "Отправление логина (I) и публичного ключа (A)",
            },
            channel: {
              key: "Передача данных",
              value: `{login: "${this.login}", A: "${A.toString(16)}"}`
            },
            server: {
              full: "Получение данных"
            }
          });
          let a1 = this.$parent.$children[1].auth1(this.login, A.toString(16));
          if (a1.Error) {
            this.answer = `[${this.getCurrentTime()}] ${a1.Error}`;
          }
          else {
            srp.setB(bigInt(a1.B, 16));
            srp.setSalt(a1.salt);
            let tmp_x = srp.calcx();
            this.$parent.$children[2].add({
              client: {
                key: "Вычисление хэша логина и пароля с солью (x)",
                value: `${tmp_x}`
              }
            });
            let tmp_u = srp.calcu();
            this.$parent.$children[2].add({
              client: {
                key: "Вычисление хэша публичных ключей A и B (u)",
                value: `${tmp_u}`
              }
            });
            let k = srp.calcClientKey();
            this.$parent.$children[2].add({
              client: {
                key: "Вычисление ключа сессии (K)",
                value: `${k}`
              }
            });
            this.$parent.$children[2].add({
              client: {
                full: "Отправление логина (I) и ключа сессии (K)",
              },
              channel: {
                key: "Передача данных",
                value: `{login: "${this.login}", K: "${k}"}`
              },
              server: {
                full: "Получение данных"
              }
            });
            let a2 = this.$parent.$children[1].auth2(this.login, k);
            if (a2.Error) {
              this.answer = `[${this.getCurrentTime()}] ${a2.Error}`;
            }
            else {
              this.answer = `[${this.getCurrentTime()}] Успешно`;
            }
          }
        }
      },

      getCurrentTime() {
        let d = new Date();
        return `${this.leadZero(d.getHours().toString(), 2)}:${this.leadZero(d.getMinutes().toString(), 2)}:${this.leadZero(d.getSeconds().toString(), 2)}.${this.leadZero(d.getMilliseconds().toString(), 3)}`;
      },

      leadZero(str, length) {
        while (str.length < length) {
          str = "0" + str;
        }
        return str;
      }
    }
  });

  return clientComponent;

}