function getCalculationsComponent() {

  let calculationsComponent = Vue.component("calculations", {

    data: function () {
      return {
        row: []
      }
    },

    template: `<div class="calculations">
      <div class="row header">
        <div class="client col">
          Клиент
        </div>
        <div class="channel col">
          Канал
        </div>
        <div class="server col">
          Сервер
        </div>
      </div>
      <div class="row" v-for="i in row">
        <div class="client col" v-if="i.client">
          <div class="key" v-if="!i.client.full">{{i.client.key}}</div>
          <div class="value" v-if="!i.client.full">{{i.client.value}}</div>
          <div class="full" v-if="i.client.full">{{i.client.full}}</div>
        </div>
        <div class="channel col" v-if="i.channel">
          <div class="key" v-if="!i.channel.full">{{i.channel.key}}</div>
          <div class="value" v-if="!i.channel.full">{{i.channel.value}}</div>
          <div class="full" v-if="i.channel.full">{{i.channel.full}}</div>
        </div>
        <div class="server col" v-if="i.server">
          <div class="key" v-if="!i.server.full">{{i.server.key}}</div>
          <div class="value" v-if="!i.server.full">{{i.server.value}}</div>
          <div class="full" v-if="i.server.full">{{i.server.full}}</div>
        </div>
      </div>
    </div>`,

    methods: {
      add(obj) {
        if (!obj.client) {
          obj = {...obj, client: {full: ""}};
        }
        if (!obj.channel) {
          obj = {...obj, channel: {full: ""}};
        }
        if (!obj.server) {
          obj = {...obj, server: {full: ""}};
        }
        this.row.push(obj);
      },

      clear() {
        this.row = [];
      }
    }
  });

  return calculationsComponent;

}