function getServerComponent() {

  let serverComponent = Vue.component("server", {

    data: function () {
      return {
        user: {},
        srp: null
      }
    },

    template: `<div class="server">
      <table class="table">
        <thead>
          <tr>
            <td>Логин</td>
            <td>Соль</td>
            <td class="dynamic">Проверочное значение</td>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(i,index) in user" v-bind:title="i.password">
            <td>{{index}}</td>
            <td>{{i.salt}}</td>
            <td class="verifier"><div class="number">{{i.v}}</div></td>
          </tr>
        </tbody>
      </table>
    </div>`,

    mounted() {
    },

    methods: {
      reg(login, password, hash) {
        let s = getRandomString(10, 20);
        this.$parent.$children[2].add({
          server: {
            key: "Генерирование соли (s)",
            value: s
          }
        });
        let obj = {};
        obj.password = password;
        obj.salt = s;
        let srp = new SRP(4096);
        let hs = srp.H(s);
        let x = srp.H(`${hash}${hs}`);
        this.$parent.$children[2].add({
          server: {
            key: "Вычисление хеша логина (I) и пароля (p) с солью (s)",
            value: x
          }
        });
        obj.v = srp.g.modPow(bigInt(x, 16), srp.N).toString();
        this.$parent.$children[2].add({
          server: {
            key: "Вычисление проверочного значения (v)",
            value: obj.v.toString()
          }
        });
        if (this.user[login]) {
          this.$parent.$children[2].add({
            server: {
              full: `Обновление данных пользователя ${login}`
            }
          });
        }
        else {
          this.$parent.$children[2].add({
            server: {
              full: `Добавление нового пользователя ${login}`
            }
          });
        }
        this.$set(this.user, login, obj);
      },

      auth1(login, A) {
        if (!this.user[login]) {
          this.$parent.$children[2].add({
            server: {
              full: `Пользователь ${login} не найден`
            }
          });
          return {Error: "User not found"}
        }
        else {
          this.srp = new SRP(4096);
          this.srp.setLogin(login);
          this.srp.setA(bigInt(A, 16));
          this.srp.setV(bigInt(this.user[login].v));
          let tmp_b = this.srp.genb();
          this.$parent.$children[2].add({
            server: {
              key: `Генерирование секретного ключа (b)`,
              value: `${tmp_b}`
            }
          });
          let B = this.srp.calcB().toString(16);
          this.$parent.$children[2].add({
            server: {
              key: `Вычисление публичного ключа (B)`,
              value: `${B.toString(16)}`
            }
          });
          this.$parent.$children[2].add({
            server: {
              full: "Отправление соли (s) и публичного ключа (B)"
            },
            channel: {
              key: `Передача данных`,
              value: `{salt: "${this.user[login].salt}", B: "${B.toString(16)}"}`
            },
            client: {
              full: "Получение данных"
            }
          });
          return {
            salt: this.user[login].salt,
            B: B
          };
        }
      },

      auth2(login, key) {
        if (!this.user[login]) {
          return {Error: "User not found"}
        }
        else {
          let tmp_u = this.srp.calcu();
          this.$parent.$children[2].add({
            server: {
              key: "Вычисление хэша публичных ключей A и B (u)",
              value: `${tmp_u}`
            }
          });
          let k = this.srp.calcServerKey();
          this.$parent.$children[2].add({
            server: {
              key: "Вычисление ключа сессии (K)",
              value: `${k}`
            }
          });
          if (k == key) {
            this.$parent.$children[2].add({
              server: {
                full: "Ключи совпали, доступ разрешен"
              }
            });
            return {access: true};
          }
          else {
            this.$parent.$children[2].add({
              server: {
                full: "Ключи не совпали, доступ запрещен"
              }
            });
            return {Error: "Wrong username or password"};
          }
        }
      }
    }
  });

  return serverComponent;

}