# srp-6a-js-demo

В репозитории показан один из возможных вариантов реализации протокола SRP-6a на JavaScript. Для реализации использовался фреймворк [VueJS](https://vuejs.org/), а также пакет [js-sha256](https://www.npmjs.com/package/js-sha256), реализующие одностороннюю функцию хэширования SHA256 и библиотека [BigInteger](https://github.com/peterolson/BigInteger.js/), реализующая длинную арифметику.
